# Test Case 1: check all alarm functions with valid inputs

| Test Step    | Expected Result|
| ----------- | ----------- |
|  Compile alarm.c and run the program ```./alarm```. Make sure there is sample1.mp3 file in the current directory. |    |
|<ul><li>schedule new alarm by printing ```s``` and then alarm time some minutes after the current time in format ```yyyy-mm-dd tt:mm:ss```<li>schedule one more alarm some minutes after the previous <li>print ```l```  </ul>| Both alarms with unique numbers (1 and 2 ) are displayed       |
|Print ```c```|```Cancel which alarm?``` is displayed |
|Print ```1```, print ```l```|Only the last sett alarm is displayed (with number 1)|
|Print ```x```|```Goodbye!``` is displayed. Program is finished.|
|Wait for the alarm to ring at the right time.|`<ul><li>``RING!``` is displayed. </li><li>If there is sample1.mp3 in the current directory it will be played.</li></ul>|


# Test Case 2: check there is no zombies after alarms have rung

| Test Step    | Expected Result|
| ----------- | ----------- |
|  Compile alarm.c by running ```make``` and run the program ```./alarm```.|    |
|<ul><li>schedule new alarm by printing ```s``` and then alarm time some minutes after the current time in format ```yyyy-mm-dd tt:mm:ss```<li>schedule one more alarm some minutes after the previous <li>print ```l```  </ul>| Both alarms with unique numbers (1 and 2 ) are displayed       |
|Print ```x```|```Goodbye!``` is displayed. Program is finished.|
|Wait for the alarms to ring at the right time. After both alarms have rung print ```ps```|There is no processes related to alarm (CMD = alarm)|


# Test Case 3: check you are not allowed to schedule alarm for the past and not allowed to cancel non-existing alarm
| Test Step    | Expected Result|
| ----------- | ----------- |
|  Compile alarm.c and run the program ```./alarm```.|    |
|Schedule new alarm by printing ```s``` and then alarm time some minutes after the current time in format ```yyyy-mm-dd tt:mm:ss```Make sure the alarm time has already past. |   ```Alarms for the past are not allowded``` is displayed  |
|chedule new alarm by printing ```s``` and then alarm time some minutes after the current time in format ```yyyy-mm-dd tt:mm:ss```. Print ```c```|```Cancel which alarm?``` is displayed |
|Print any number except the right alarm number (except 1)| ```Alarm $(printed number) does not exist``` is displayed |
|Print ```x```|```Goodbye!``` is displayed. Program is finished.|


# Test Case 4: check that alarms that has rung are not listed as currently active
| Test Step    | Expected Result|
| ----------- | ----------- |
|  Compile alarm.c and run the program ```./alarm```. |    |
|<ul><li>schedule new alarm by printing ```s``` and then alarm time some minutes after the current time in format ```yyyy-mm-dd tt:mm:ss```<li>schedule one more alarm some minutes after the previous <li>print ```l```  </ul>| Both alarms with unique numbers (1 and 2 ) are displayed       |
|Wait for the first alarm to ring, print ```l``` |Only the last sett alarm is displayed (with number 1)|
|Wait for the second alarm to ring, print ```l``` |No alarms are displayed|
|Print ```x```|```Goodbye!``` is displayed. Program is finished.|



